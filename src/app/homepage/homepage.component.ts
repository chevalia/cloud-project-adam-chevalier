import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { HttpClient } from '@angular/common/http';
import { Chart } from 'chart.js';
import { dataSummary } from '../dataSummary.model';
import { dailyData } from '../dailyData.model';
import { News } from '../news.model';
import * as moment from 'moment';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {

  News: News[];
  globaldata$: dataSummary;
  countryData$: dataSummary[];
  dailyData$: dailyData[];
  aprilData$: dailyData[];

  PieChart = [];
  BarChart = [];

  activeCases: number;
  recoveryRate: string;
  mortalityRate: string;

  myDate: any;

  country = 1;
  newConf = -1;
  totalConf = -1;
  newReco = -1;
  totalReco = -1;
  newDeat = -1;
  totalDeat = -1;

  constructor(public covidService: CovidService, private http: HttpClient) { }

  ngOnInit(): void {
    this.News = [];
    this.covidService.getNews("Worldwide")
      .subscribe((news: News[]) => {
        this.News = news;
      });
    if (this.News.length == 0) {
      this.News.push({ uid: "", date: "", displayName: "", description: "No news for the moment", country: "" })
    }

    this.globaldata$ = {
      "Country": "All", "Date": "Today", "NewConfirmed": 0, "TotalConfirmed": 0, "NewDeaths": 0, "TotalDeaths": 0,
      "NewRecovered": 0, "TotalRecovered": 0
    };
    this.countryData$ = [];
    this.dailyData$ = [];
    this.aprilData$ = [];

    this.globalData();
    this.dailyData();
    this.getLastSevenDays();
    this.getSinceAprilData();

  }

  private globalData() {
    this.http.get('https://api.covid19api.com/summary').subscribe(Response => {
      const res = JSON.parse(JSON.stringify(Response)).Global;
      this.globaldata$ = {
        Country: "All",
        Date: moment().format("DD-MM-YYYY"),
        NewConfirmed: parseInt(res.NewConfirmed),
        TotalConfirmed: parseInt(res.TotalConfirmed),
        NewDeaths: parseInt(res.NewDeaths),
        TotalDeaths: parseInt(res.TotalDeaths),
        NewRecovered: parseInt(res.NewRecovered),
        TotalRecovered: parseInt(res.TotalRecovered)
      }

      this.activeCases = this.globaldata$.TotalConfirmed -
        this.globaldata$.TotalRecovered - this.globaldata$.TotalDeaths;

      this.recoveryRate = String(Math.round(this.globaldata$.TotalRecovered /
        this.globaldata$.TotalConfirmed * 10000) / 100) + "%";

      this.mortalityRate = String(Math.round(this.globaldata$.TotalDeaths /
        this.globaldata$.TotalConfirmed * 10000) / 100) + "%";

      var pieData = [this.globaldata$.TotalDeaths,
      this.globaldata$.TotalRecovered, this.activeCases];

      this.PieChart = new Chart('pieChart', {
        type: 'pie',
        data: {
          labels: ["Dead Cases", "Recovered Cases", "Active Cases"],
          datasets: [{
            data: pieData,
            backgroundColor: [
              '#eb8ca2',
              '#7bc1f2',
              '#fddf93'
            ],
          }]
        }
      });

    });
  }

  private dailyData() {
    this.http.get('https://api.covid19api.com/summary').subscribe(Response => {
      const res = JSON.parse(JSON.stringify(Response)).Countries;
      var countryData: dataSummary;
      for (let i = 0; i < res.length; i++) {
        countryData = {
          Country: res[i].Country,
          Date: moment().subtract(8 - i, 'd').format("DD-MM-YYYY"),
          NewConfirmed: parseInt(res[i].NewConfirmed),
          TotalConfirmed: parseInt(res[i].TotalConfirmed),
          NewDeaths: parseInt(res[i].NewDeaths),
          TotalDeaths: parseInt(res[i].TotalDeaths),
          NewRecovered: parseInt(res[i].NewRecovered),
          TotalRecovered: parseInt(res[i].TotalRecovered)
        }
        this.countryData$.push(countryData);
      }
    });
  }

  private getLastSevenDays() {
    this.http.get('https://corona.lmao.ninja/v2/historical/all').subscribe(Response => {
      const res = JSON.parse(JSON.stringify(Response));
      let len = 30;
      for (let i = len-7; i < len; i++) {
        var oneDay: dailyData;
        let date = moment().subtract(len-i,"d").format("M/D/YY");
        let datebefore = moment().subtract(len-i+1,"d").format("M/D/YY");
        oneDay = {
          Date: moment().subtract(len-i,"d").format("DD-MM-YYYY"),
          Confirmed: res.cases[date]-res.cases[datebefore],
          Deaths: res.deaths[date]-res.deaths[datebefore],
          Recovered: res.recovered[date]-res.recovered[datebefore]
        }
        this.dailyData$.push(oneDay);
      }

      if (res.length < 7) {
        for (let i = 0; i < 7 - res.length; i++) {
          var oneDay: dailyData;
          oneDay = {
            Date: "date",
            Confirmed: 0,
            Deaths: 0,
            Recovered: 0
          }
          this.dailyData$.push(oneDay);
        }
      }

      let daysBar: string[] = [];
      for (let i = 0; i < 7; i++) {
        daysBar.push(moment().subtract(7 - i, 'd').format("DD MMM"));
      }
      var dataBar = {
        labels: daysBar,
        datasets: [
          {
            label: "Daily Deaths",
            data: [this.dailyData$[0].Deaths, this.dailyData$[1].Deaths, this.dailyData$[2].Deaths,
            this.dailyData$[3].Deaths, this.dailyData$[4].Deaths, this.dailyData$[5].Deaths,
            this.dailyData$[6].Deaths],
            borderWidth: 1,
            backgroundColor: '#eb8ca2'
          },
          {
            label: "Daily Recovered",
            data: [this.dailyData$[0].Recovered, this.dailyData$[1].Recovered,
            this.dailyData$[2].Recovered, this.dailyData$[3].Recovered,
            this.dailyData$[4].Recovered, this.dailyData$[5].Recovered,
            this.dailyData$[6].Recovered],
            borderWidth: 1,
            backgroundColor: '#7bc1f2'
          },
          {
            label: "Daily New Cases",
            data: [this.dailyData$[0].Confirmed, this.dailyData$[1].Confirmed,
            this.dailyData$[2].Confirmed, this.dailyData$[3].Confirmed,
            this.dailyData$[4].Confirmed, this.dailyData$[5].Confirmed,
            this.dailyData$[6].Confirmed],
            borderWidth: 1,
            backgroundColor: '#fddf93'
          }
        ]
      };

      var optionsBar = {
        legend: {
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 0
            }
          }]
        }
      };

      var chart = new Chart(document.getElementById("barChart"), {
        type: "bar",
        data: dataBar,
        options: optionsBar
      });

    })
  }

  private getSinceAprilData() {
    this.http.get('https://corona.lmao.ninja/v2/historical/all').subscribe(Response => {
      const res = JSON.parse(JSON.stringify(Response));
      let len = 30;
      for (let i = 0; i < len; i++) {
        var oneDay: dailyData;
        let date = moment().subtract(len-i,"d").format("M/D/YY");
        oneDay = {
          Date: moment().subtract(len-i,"d").format("DD-MM-YYYY"),
          Confirmed: res.cases[date],
          Deaths: res.deaths[date],
          Recovered: res.recovered[date]
        }
        this.aprilData$.push(oneDay);
      }

      let daysLine: string[] = [];
      let daysDeath: number[] = [];
      let daysRecovered: number[] = [];
      let daysCases: number[] = [];

      for (let i = 0; i < this.aprilData$.length; i++) {
        daysLine.push(this.aprilData$[i].Date);
        daysDeath.push(this.aprilData$[i].Deaths);
        daysRecovered.push(this.aprilData$[i].Recovered);
        daysCases.push(this.aprilData$[i].Confirmed);
      }


      var dataLine = {
        labels: daysLine,
        datasets: [
          {
            label: "Total Deaths",
            data: daysDeath,
            borderWidth: 1,
            backgroundColor: '#eb8ca2'
          },
          {
            label: "Total Recovered",
            data: daysRecovered,
            borderWidth: 1,
            backgroundColor: '#7bc1f2'
          },
          {
            label: "Total Cases",
            data: daysCases,
            borderWidth: 1,
            backgroundColor: '#fddf93'
          }
        ]
      };

      var optionsLine = {
        legend: {
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 0
            }
          }]
        }
      };

      var chart = new Chart(document.getElementById("lineChart"), {
        type: "line",
        data: dataLine,
        options: optionsLine
      });

    })


  }

  public changeArrow(id: string) {
    this.resetArrow();
    if (id == "country") {
      this.country *= -1;
      if (this.country == 1) {
        document.getElementById("country_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("country_D").className = "las la-angle-double-down";
      }
    } else if (id == "newConf") {
      this.newConf *= -1;
      if (this.newConf == 1) {
        document.getElementById("newConf_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("newConf_D").className = "las la-angle-double-down";
      }
    } else if (id == "totalConf") {
      this.totalConf *= -1;
      if (this.totalConf == 1) {
        document.getElementById("totalConf_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("totalConf_D").className = "las la-angle-double-down";
      }
    } else if (id == "newReco") {
      this.newReco *= -1;
      if (this.newReco == 1) {
        document.getElementById("newReco_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("newReco_D").className = "las la-angle-double-down";
      }
    } else if (id == "totalReco") {
      this.totalReco *= -1;
      if (this.totalReco == 1) {
        document.getElementById("totalReco_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("totalReco_D").className = "las la-angle-double-down";
      }
    } else if (id == "newDeat") {
      this.newDeat *= -1;
      if (this.newDeat == 1) {
        document.getElementById("newDeat_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("newDeat_D").className = "las la-angle-double-down";
      }
    } else if (id == "totalDeat") {
      this.totalDeat *= -1;
      if (this.totalDeat == 1) {
        document.getElementById("totalDeat_U").className = "las la-angle-double-up";
      } else {
        document.getElementById("totalDeat_D").className = "las la-angle-double-down";
      }
    }
  }

  private resetArrow() {
    const idArrowsUp = ["country_U", "newConf_U", "totalConf_U", "newReco_U", "totalReco_U", "newDeat_U", "totalDeat_U"];
    const idArrowsDown = ["country_D", "newConf_D", "totalConf_D", "newReco_D", "totalReco_D", "newDeat_D", "totalDeat_D"];

    for (let id of idArrowsUp) {
      document.getElementById(id).className = "las la-angle-up";
    }
    for (let id of idArrowsDown) {
      document.getElementById(id).className = "las la-angle-down";
    }
  }

  public displayNews() {
    document.getElementById("displayNews").hidden = true;
    document.getElementById("newsFrame").hidden = false;
  }

  public hideNews() {
    document.getElementById("displayNews").hidden = false;
    document.getElementById("newsFrame").hidden = true;
  }

}