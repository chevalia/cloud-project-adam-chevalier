import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { User } from '../user.model';
import { HttpClient } from '@angular/common/http';
import { News } from '../news.model';
import { country } from '../country.model';
import * as moment from 'moment';

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  user: User;
  description: string;
  country: string;

  Countries$: country[];

  constructor(public covidService: CovidService, private http: HttpClient) { }

  ngOnInit(): void {
    this.country = "Worldwide";

    this.user = this.covidService.getUser();
    
    this.Countries$ = [];
    this.addCountryToSelect();
  }

  addNews() {
    if (!this.user.eligible) {
      document.getElementById("notEligible").hidden = false;
      return;
    }


    let str = moment().format("DD-MM-YYYY");
    if (this.description == "" || this.description==undefined) {
      document.getElementById("newsMessage").hidden = true;
      return;
    }
    let news: News = {
      uid : this.user.uid,
      date :  str,
      displayName : this.user.displayName,
      description : this.description,
      country : this.country
    } 
    this.covidService.addNews(news);
    document.getElementById("newsMessage").hidden = false;
    this.description = undefined;

  }

  private addCountryToSelect() {
    var select = document.getElementById('countryInput');
    this.http.get('https://api.covid19api.com/countries').subscribe(Response => {
      const res = JSON.parse(JSON.stringify(Response));
      let Country = [];
      for (let i = 0; i < res.length; i++) {
        var countryOption : country;
        countryOption = {
          title: res[i].Country, 
          value: res[i].Country
        } 
        this.Countries$.push(countryOption);
      }
      this.Countries$.sort((a, b) => a.title.localeCompare(b.title))
    });
  }

  public beEligible() {
    this.covidService.beEligible();
    this.user.eligible = true;
    document.getElementById("notEligible").hidden = true;
  }

}
