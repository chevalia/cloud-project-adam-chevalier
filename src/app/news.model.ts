export interface News {
  uid: string;
  date : any;
  displayName: string;
  description: string;
  country: string;
}