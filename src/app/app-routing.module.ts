import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { CountryComponent } from './country/country.component';
import { AuthGuard } from './auth.guard';
import { SecurePagesGuard } from './secure-pages.guard';
import { CountryPagesGuard } from './country-pages.guard';


const routes: Routes = [
  { path: "homepage", component: HomepageComponent,
  canActivate: [SecurePagesGuard] },
  { path: "add-news", component: AddNewsComponent, 
  canActivate: [AuthGuard] },
  { path: "", pathMatch: "full", redirectTo: "homepage" },
  { path: "**", component: CountryComponent,
  canActivate: [CountryPagesGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
