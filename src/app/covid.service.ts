import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { User } from './user.model';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { News } from './news.model'
import { dataSummary } from './dataSummary.model';
import { uid } from './uid.model';


@Injectable({
  providedIn: 'root'
})
export class CovidService {

  private user: User;

  constructor(private afAuth: AngularFireAuth, private router: Router,
    private firestore: AngularFirestore, private httpClient: HttpClient) { }

  async signInWithGoogle() {
    const credentials = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());

    this.firestore.collection("eligible").valueChanges()
      .subscribe((eligibles: uid[]) => {
        let isEligible = false;
        for (let i = 0; i < eligibles.length; i++) {
          if (credentials.user.uid == eligibles[i].uid) {
            isEligible = true;
          }
        }
        if (isEligible) {
          this.user = {
            uid: credentials.user.uid,
            displayName: credentials.user.displayName,
            email: credentials.user.email,
            eligible: true
          };
        } else {
          this.user = {
            uid: credentials.user.uid,
            displayName: credentials.user.displayName,
            email: credentials.user.email,
            eligible: false
          };
        }
        localStorage.setItem("user", JSON.stringify(this.user));
        this.updateUserData();
        this.router.navigate(["add-news"])
      })
  }

  private updateUserData() {
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email,
      eligible: true
    }, { merge: true });
  }

  getUser() {
    if (this.user == null && this.userSignedIn()) {
      this.user = JSON.parse(localStorage.getItem("user"));
    }
    return this.user;
  }

  userSignedIn(): boolean {
    return JSON.parse(localStorage.getItem("user")) != null;
  }

  signOut() {
    this.afAuth.signOut();
    localStorage.removeItem("user");
    this.user = null;
    this.router.navigate(["homepage"]);
  }

  openCountry(country: String) {
    localStorage.setItem("country", JSON.stringify(country));
    this.router.navigate([country])
  }

  onCountry(): boolean {
    return JSON.parse(localStorage.getItem("country")) != null;
  }

  toHomepage() {
    localStorage.removeItem("country");
    this.router.navigate(["homepage"]);
  }

  addNews(news: News) {
    this.firestore.collection("country").doc(news.country).collection("news").add(news);
  }

  getNews(country: string) {
    return this.firestore.collection("country")
      .doc(country).collection("news").valueChanges();
  }

  getSummary(country: string) {
    return this.firestore.collection("country")
      .doc(country).collection("summary").doc("summary").valueChanges();
  }

  updateSummary(country: string, summary: dataSummary) {
    this.firestore.collection("country").doc(country).collection("summary").doc("summary").set(summary, { merge: true });
  }

  beEligible() {
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email,
      eligible: true
    }, { merge: true });
    this.firestore.collection("eligible").add({ uid: this.user.uid });
  }

}
