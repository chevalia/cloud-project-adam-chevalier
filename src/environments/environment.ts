// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDlvomAv4gmq9OCC_DnloDJvc7Jkib-q4c",
    authDomain: "covidproject-89200.firebaseapp.com",
    databaseURL: "https://covidproject-89200.firebaseio.com",
    projectId: "covidproject-89200",
    storageBucket: "covidproject-89200.appspot.com",
    messagingSenderId: "83215962915",
    appId: "1:83215962915:web:3441fa118351475b837296",
    measurementId: "G-GKY3YCS804"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
